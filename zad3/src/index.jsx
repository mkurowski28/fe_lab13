import React from "react";
import ReactDOM from "react-dom";
import "./style.css";

const App = () => {
  return (
    <div>
      <h1>Hello React!</h1>
      <img src={require("./images/example.jpg")} alt="Sample" />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
