"use client";
import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
function Zad1() {
  const [counter, setData] = useState(0);
  const handleLeftClick = (event) => {
    setData((counter) => counter + 1);
  };
  const handleRightClick = (event) => {
    setData((counter) => counter - 1);
  };
  useEffect(() => {
    window.addEventListener("click", handleLeftClick);
    return () => {
      window.removeEventListener("click", handleLeftClick);
    };
  }, []);
  useEffect(() => {
    window.addEventListener("contextmenu", handleRightClick);
    return () => {
      window.removeEventListener("contextmenu", handleRightClick);
    };
  }, []);

  return (
    <div>
      aaaaaaaaaaaa
      <div className="text-blue-600 flex justify-center items-center h-screen align-middle text-9xl">
        <div className="counter">{counter}</div>
        aaaaaaaaaaaaaaaaaaaaaa
      </div>
    </div>
  );
}
ReactDOM.render(<Zad1 />, document.getElementById("root"));
